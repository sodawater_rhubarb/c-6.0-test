﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld2
{
    class Program
    {
        public static string Truncate(string value, int length)
        {
            return value?.Substring(0, Math.Min(value.Length, length));
        }

        static void Main(string[] args)
        {
            Console.Write(Truncate("Hello World", 5));
            var b = 6;
            var a = $"b is {b}";

            var c = new TestClass();
            var childName = c?.Child?.Child?.Child.ID;

        }
    }

    class TestClass
    {
        public TestClass Child { get; set; }
        public int ID { get; set; }
    }
}
